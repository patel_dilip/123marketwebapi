package com.rb.core.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class MasterSheetBean {
	
    long id;
    String masterSheetNo;
    String invoiceNo;
    String buyersPONo;
    String PINo;
    String customerFullName;
    String address1;
    String address2;
    String city;
    String country;
    String GSTNo;
    String IECNo;
    String emailId;
    String incoTerms;
    String POL;
    String POD;
    String finalDestination;
    String countryOfOrigin;
    String currency;
    String ourBankDetails;
    String paymentTerms;
    String additionalConditions;
    String partialDelivery;
    String transShipment;
    String legalisation;
    String LCExpiryPlace;
    String documentsProvided;
    String DNNo;
    String LCNo;
    String LPONo;
    String HSCode;
    String grossWeightUnit;
    String netWeightUnit;
    String manufacturerProducer;
    String LCNo_1;
    String LCOpeningBank;
    String LCApplicant;
    String DraftsToBeDrawnOn;
    String PLNo;
    String BINNo;
    String applicantTINNo;
    String bankTINNo;
    String BOENo;
    String BLNo;
    String shippingLine;
    String vesselName;
    String insurancePolicyNo;
    String coverNoteNo;
    String otherConditions1;
    String otherConditions2;
    String otherConditions3;
    String otherConditions4;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MM/dd/yyyy")
    Date invoiceDate;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MM/dd/yyyy")
    Date buyersPODate;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MM/dd/yyyy")
    Date PIDate;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MM/dd/yyyy")
    Date latestShipmentDate;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MM/dd/yyyy")
    Date deliveryDate;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MM/dd/yyyy")
    Date LCDate;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MM/dd/yyyy")
    Date LCExpiryDate;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MM/dd/yyyy")
    Date PLDate;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MM/dd/yyyy")
    Date BOEDate;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MM/dd/yyyy")
    Date BLDate;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MM/dd/yyyy")
    Date ETDDate;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MM/dd/yyyy")
    Date ETADate;
    Double totalAmount;
    Double freight;
    Double otherCharges;
    Double amount;
    int tolerance;
    int grossWeight;
    int netWeight;
    
    public MasterSheetBean() {super();}
    
	public MasterSheetBean(long id, String masterSheetNo, String invoiceNo, String buyersPONo, String pINo,
			String customerFullName, String address1, String address2, String city, String country, String gSTNo,
			String iECNo, String emailId, String incoTerms, String pOL, String pOD, String finalDestination,
			String countryOfOrigin, String currency, String ourBankDetails, String paymentTerms,
			String additionalConditions, String partialDelivery, String transShipment, String legalisation,
			String lCExpiryPlace, String documentsProvided, String dNNo, String lCNo, String lPONo, String hSCode,
			String grossWeightUnit, String netWeightUnit, String manufacturerProducer, String lCNo_1,
			String lCOpeningBank, String lCApplicant, String draftsToBeDrawnOn, String pLNo, String bINNo,
			String applicantTINNo, String bankTINNo, String bOENo, String bLNo, String shippingLine, String vesselName,
			String insurancePolicyNo, String coverNoteNo, String otherConditions1, String otherConditions2,
			String otherConditions3, String otherConditions4, Date invoiceDate, Date buyersPODate, Date pIDate,
			Date latestShipmentDate, Date deliveryDate, Date lCDate, Date lCExpiryDate, Date pLDate, Date bOEDate,
			Date bLDate, Date eTDDate, Date eTADate, Double totalAmount, Double freight, Double otherCharges,
			Double amount, int tolerance, int grossWeight, int netWeight) {
		super();
		this.id = id;
		this.masterSheetNo = masterSheetNo;
		this.invoiceNo = invoiceNo;
		this.buyersPONo = buyersPONo;
		PINo = pINo;
		this.customerFullName = customerFullName;
		this.address1 = address1;
		this.address2 = address2;
		this.city = city;
		this.country = country;
		GSTNo = gSTNo;
		IECNo = iECNo;
		this.emailId = emailId;
		this.incoTerms = incoTerms;
		POL = pOL;
		POD = pOD;
		this.finalDestination = finalDestination;
		this.countryOfOrigin = countryOfOrigin;
		this.currency = currency;
		this.ourBankDetails = ourBankDetails;
		this.paymentTerms = paymentTerms;
		this.additionalConditions = additionalConditions;
		this.partialDelivery = partialDelivery;
		this.transShipment = transShipment;
		this.legalisation = legalisation;
		LCExpiryPlace = lCExpiryPlace;
		this.documentsProvided = documentsProvided;
		DNNo = dNNo;
		LCNo = lCNo;
		LPONo = lPONo;
		HSCode = hSCode;
		this.grossWeightUnit = grossWeightUnit;
		this.netWeightUnit = netWeightUnit;
		this.manufacturerProducer = manufacturerProducer;
		LCNo_1 = lCNo_1;
		LCOpeningBank = lCOpeningBank;
		LCApplicant = lCApplicant;
		DraftsToBeDrawnOn = draftsToBeDrawnOn;
		PLNo = pLNo;
		BINNo = bINNo;
		this.applicantTINNo = applicantTINNo;
		this.bankTINNo = bankTINNo;
		BOENo = bOENo;
		BLNo = bLNo;
		this.shippingLine = shippingLine;
		this.vesselName = vesselName;
		this.insurancePolicyNo = insurancePolicyNo;
		this.coverNoteNo = coverNoteNo;
		this.otherConditions1 = otherConditions1;
		this.otherConditions2 = otherConditions2;
		this.otherConditions3 = otherConditions3;
		this.otherConditions4 = otherConditions4;
		this.invoiceDate = invoiceDate;
		this.buyersPODate = buyersPODate;
		PIDate = pIDate;
		this.latestShipmentDate = latestShipmentDate;
		this.deliveryDate = deliveryDate;
		LCDate = lCDate;
		LCExpiryDate = lCExpiryDate;
		PLDate = pLDate;
		BOEDate = bOEDate;
		BLDate = bLDate;
		ETDDate = eTDDate;
		ETADate = eTADate;
		this.totalAmount = totalAmount;
		this.freight = freight;
		this.otherCharges = otherCharges;
		this.amount = amount;
		this.tolerance = tolerance;
		this.grossWeight = grossWeight;
		this.netWeight = netWeight;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getMasterSheetNo() {
		return masterSheetNo;
	}
	public void setMasterSheetNo(String masterSheetNo) {
		this.masterSheetNo = masterSheetNo;
	}
	public String getInvoiceNo() {
		return invoiceNo;
	}
	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}
	public String getBuyersPONo() {
		return buyersPONo;
	}
	public void setBuyersPONo(String buyersPONo) {
		this.buyersPONo = buyersPONo;
	}
	public String getPINo() {
		return PINo;
	}
	public void setPINo(String pINo) {
		PINo = pINo;
	}
	public String getCustomerFullName() {
		return customerFullName;
	}
	public void setCustomerFullName(String customerFullName) {
		this.customerFullName = customerFullName;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getGSTNo() {
		return GSTNo;
	}
	public void setGSTNo(String gSTNo) {
		GSTNo = gSTNo;
	}
	public String getIECNo() {
		return IECNo;
	}
	public void setIECNo(String iECNo) {
		IECNo = iECNo;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getIncoTerms() {
		return incoTerms;
	}
	public void setIncoTerms(String incoTerms) {
		this.incoTerms = incoTerms;
	}
	public String getPOL() {
		return POL;
	}
	public void setPOL(String pOL) {
		POL = pOL;
	}
	public String getPOD() {
		return POD;
	}
	public void setPOD(String pOD) {
		POD = pOD;
	}
	public String getFinalDestination() {
		return finalDestination;
	}
	public void setFinalDestination(String finalDestination) {
		this.finalDestination = finalDestination;
	}
	public String getCountryOfOrigin() {
		return countryOfOrigin;
	}
	public void setCountryOfOrigin(String countryOfOrigin) {
		this.countryOfOrigin = countryOfOrigin;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getOurBankDetails() {
		return ourBankDetails;
	}
	public void setOurBankDetails(String ourBankDetails) {
		this.ourBankDetails = ourBankDetails;
	}
	public String getPaymentTerms() {
		return paymentTerms;
	}
	public void setPaymentTerms(String paymentTerms) {
		this.paymentTerms = paymentTerms;
	}
	public String getAdditionalConditions() {
		return additionalConditions;
	}
	public void setAdditionalConditions(String additionalConditions) {
		this.additionalConditions = additionalConditions;
	}
	public String getPartialDelivery() {
		return partialDelivery;
	}
	public void setPartialDelivery(String partialDelivery) {
		this.partialDelivery = partialDelivery;
	}
	public String getTransShipment() {
		return transShipment;
	}
	public void setTransShipment(String transShipment) {
		this.transShipment = transShipment;
	}
	public String getLegalisation() {
		return legalisation;
	}
	public void setLegalisation(String legalisation) {
		this.legalisation = legalisation;
	}
	public String getLCExpiryPlace() {
		return LCExpiryPlace;
	}
	public void setLCExpiryPlace(String lCExpiryPlace) {
		LCExpiryPlace = lCExpiryPlace;
	}
	public String getDocumentsProvided() {
		return documentsProvided;
	}
	public void setDocumentsProvided(String documentsProvided) {
		this.documentsProvided = documentsProvided;
	}
	public String getDNNo() {
		return DNNo;
	}
	public void setDNNo(String dNNo) {
		DNNo = dNNo;
	}
	public String getLCNo() {
		return LCNo;
	}
	public void setLCNo(String lCNo) {
		LCNo = lCNo;
	}
	public String getLPONo() {
		return LPONo;
	}
	public void setLPONo(String lPONo) {
		LPONo = lPONo;
	}
	public String getHSCode() {
		return HSCode;
	}
	public void setHSCode(String hSCode) {
		HSCode = hSCode;
	}
	public String getGrossWeightUnit() {
		return grossWeightUnit;
	}
	public void setGrossWeightUnit(String grossWeightUnit) {
		this.grossWeightUnit = grossWeightUnit;
	}
	public String getNetWeightUnit() {
		return netWeightUnit;
	}
	public void setNetWeightUnit(String netWeightUnit) {
		this.netWeightUnit = netWeightUnit;
	}
	public String getManufacturerProducer() {
		return manufacturerProducer;
	}
	public void setManufacturerProducer(String manufacturerProducer) {
		this.manufacturerProducer = manufacturerProducer;
	}
	public String getLCNo_1() {
		return LCNo_1;
	}
	public void setLCNo_1(String lCNo_1) {
		LCNo_1 = lCNo_1;
	}
	public String getLCOpeningBank() {
		return LCOpeningBank;
	}
	public void setLCOpeningBank(String lCOpeningBank) {
		LCOpeningBank = lCOpeningBank;
	}
	public String getLCApplicant() {
		return LCApplicant;
	}
	public void setLCApplicant(String lCApplicant) {
		LCApplicant = lCApplicant;
	}
	public String getDraftsToBeDrawnOn() {
		return DraftsToBeDrawnOn;
	}
	public void setDraftsToBeDrawnOn(String draftsToBeDrawnOn) {
		DraftsToBeDrawnOn = draftsToBeDrawnOn;
	}
	public String getPLNo() {
		return PLNo;
	}
	public void setPLNo(String pLNo) {
		PLNo = pLNo;
	}
	public String getBINNo() {
		return BINNo;
	}
	public void setBINNo(String bINNo) {
		BINNo = bINNo;
	}
	public String getApplicantTINNo() {
		return applicantTINNo;
	}
	public void setApplicantTINNo(String applicantTINNo) {
		this.applicantTINNo = applicantTINNo;
	}
	public String getBankTINNo() {
		return bankTINNo;
	}
	public void setBankTINNo(String bankTINNo) {
		this.bankTINNo = bankTINNo;
	}
	public String getBOENo() {
		return BOENo;
	}
	public void setBOENo(String bOENo) {
		BOENo = bOENo;
	}
	public String getBLNo() {
		return BLNo;
	}
	public void setBLNo(String bLNo) {
		BLNo = bLNo;
	}
	public String getShippingLine() {
		return shippingLine;
	}
	public void setShippingLine(String shippingLine) {
		this.shippingLine = shippingLine;
	}
	public String getVesselName() {
		return vesselName;
	}
	public void setVesselName(String vesselName) {
		this.vesselName = vesselName;
	}
	public String getInsurancePolicyNo() {
		return insurancePolicyNo;
	}
	public void setInsurancePolicyNo(String insurancePolicyNo) {
		this.insurancePolicyNo = insurancePolicyNo;
	}
	public String getCoverNoteNo() {
		return coverNoteNo;
	}
	public void setCoverNoteNo(String coverNoteNo) {
		this.coverNoteNo = coverNoteNo;
	}
	public String getOtherConditions1() {
		return otherConditions1;
	}
	public void setOtherConditions1(String otherConditions1) {
		this.otherConditions1 = otherConditions1;
	}
	public String getOtherConditions2() {
		return otherConditions2;
	}
	public void setOtherConditions2(String otherConditions2) {
		this.otherConditions2 = otherConditions2;
	}
	public String getOtherConditions3() {
		return otherConditions3;
	}
	public void setOtherConditions3(String otherConditions3) {
		this.otherConditions3 = otherConditions3;
	}
	public String getOtherConditions4() {
		return otherConditions4;
	}
	public void setOtherConditions4(String otherConditions4) {
		this.otherConditions4 = otherConditions4;
	}
	public Date getInvoiceDate() {
		return invoiceDate;
	}
	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	public Date getBuyersPODate() {
		return buyersPODate;
	}
	public void setBuyersPODate(Date buyersPODate) {
		this.buyersPODate = buyersPODate;
	}
	public Date getPIDate() {
		return PIDate;
	}
	public void setPIDate(Date pIDate) {
		PIDate = pIDate;
	}
	public Date getLatestShipmentDate() {
		return latestShipmentDate;
	}
	public void setLatestShipmentDate(Date latestShipmentDate) {
		this.latestShipmentDate = latestShipmentDate;
	}
	public Date getDeliveryDate() {
		return deliveryDate;
	}
	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	public Date getLCDate() {
		return LCDate;
	}
	public void setLCDate(Date lCDate) {
		LCDate = lCDate;
	}
	public Date getLCExpiryDate() {
		return LCExpiryDate;
	}
	public void setLCExpiryDate(Date lCExpiryDate) {
		LCExpiryDate = lCExpiryDate;
	}
	public Date getPLDate() {
		return PLDate;
	}
	public void setPLDate(Date pLDate) {
		PLDate = pLDate;
	}
	public Date getBOEDate() {
		return BOEDate;
	}
	public void setBOEDate(Date bOEDate) {
		BOEDate = bOEDate;
	}
	public Date getBLDate() {
		return BLDate;
	}
	public void setBLDate(Date bLDate) {
		BLDate = bLDate;
	}
	public Date getETDDate() {
		return ETDDate;
	}
	public void setETDDate(Date eTDDate) {
		ETDDate = eTDDate;
	}
	public Date getETADate() {
		return ETADate;
	}
	public void setETADate(Date eTADate) {
		ETADate = eTADate;
	}
	public Double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}
	public Double getFreight() {
		return freight;
	}
	public void setFreight(Double freight) {
		this.freight = freight;
	}
	public Double getOtherCharges() {
		return otherCharges;
	}
	public void setOtherCharges(Double otherCharges) {
		this.otherCharges = otherCharges;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public int getTolerance() {
		return tolerance;
	}
	public void setTolerance(int tolerance) {
		this.tolerance = tolerance;
	}
	public int getGrossWeight() {
		return grossWeight;
	}
	public void setGrossWeight(int grossWeight) {
		this.grossWeight = grossWeight;
	}
	public int getNetWeight() {
		return netWeight;
	}
	public void setNetWeight(int netWeight) {
		this.netWeight = netWeight;
	}
    
    
 }
