package com.rb.core.dto;

import com.rb.core.model.MasterSheetBean;

public class MasterSheetDataDto {

	MasterSheetBean masterSheet;

	public MasterSheetBean getMasterSheet() {
		return masterSheet;
	}
	public void setMasterSheet(MasterSheetBean masterSheet) {
		this.masterSheet = masterSheet;
	}
	
}
