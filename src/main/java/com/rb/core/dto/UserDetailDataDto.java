package com.rb.core.dto;

import java.util.List;
import java.util.Map;

public class UserDetailDataDto {
	Map<String,Object> userDetailSheet;
	List<Map<String, Object>> leadDetails;
	
	
	public Map<String, Object> getUserDetailSheet() {
		return userDetailSheet;
	}
	public void setUserDetailSheet(Map<String, Object> userDetailSheet) {
		this.userDetailSheet = userDetailSheet;
	}
	public List<Map<String, Object>> getLeadDetails() {
		return leadDetails;
	}
	public void setLeadDetails(List<Map<String, Object>> leadDetails) {
		this.leadDetails = leadDetails;
	}

}
