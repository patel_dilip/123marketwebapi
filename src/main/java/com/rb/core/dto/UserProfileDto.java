package com.rb.core.dto;

import java.util.List;
import java.util.Map;

public class UserProfileDto {
	
	Map<String,Object> userProfile;
	List<Map<String, Object>> countryList;
	List<Map<String, Object>> stateList;
	List<Map<String, Object>> cityList;
	List<Map<String, Object>> phoneCC;
	
	public Map<String, Object> getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Map<String, Object> userProfile) {
		this.userProfile = userProfile;
	}
	public List<Map<String, Object>> getCountryList() {
		return countryList;
	}
	public void setCountryList(List<Map<String, Object>> countryList) {
		this.countryList = countryList;
	}
	public List<Map<String, Object>> getStateList() {
		return stateList;
	}
	public void setStateList(List<Map<String, Object>> stateList) {
		this.stateList = stateList;
	}
	public List<Map<String, Object>> getCityList() {
		return cityList;
	}
	public void setCityList(List<Map<String, Object>> cityList) {
		this.cityList = cityList;
	}
	public List<Map<String, Object>> getPhoneCC() {
		return phoneCC;
	}
	public void setPhoneCC(List<Map<String, Object>> phoneCC) {
		this.phoneCC = phoneCC;
	}
}
