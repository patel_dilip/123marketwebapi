package com.rb.core.dto;

import java.util.List;
import java.util.Map;

public class PostDataDto {
	
	Map<String,Object> postInfo;
	List<Map<String, Object>> categoryList;
	
	public Map<String, Object> getPostInfo() {
		return postInfo;
	}
	public void setPostInfo(Map<String, Object> postInfo) {
		this.postInfo = postInfo;
	}
	public List<Map<String, Object>> getCategoryList() {
		return categoryList;
	}
	public void setCategoryList(List<Map<String, Object>> categoryList) {
		this.categoryList = categoryList;
	}

}
