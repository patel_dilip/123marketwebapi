package com.rb.core.dao;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.rb.core.mapper.MasterSheetBeanMapper;
import com.rb.core.model.MasterSheetBean;

@Repository
@Transactional
public class MasterSheetDataDao extends JdbcDaoSupport {

	@Autowired
	public MasterSheetDataDao(DataSource dataSource) {
		this.setDataSource(dataSource);
	}

	public List<MasterSheetBean> findAllEntities() {

		String sql = MasterSheetBeanMapper.BASE_SQL;
		List<MasterSheetBean> resp = null;
		MasterSheetBeanMapper mapper = new MasterSheetBeanMapper();
		try {
			resp = this.getJdbcTemplate().query(sql, mapper);
		} catch (EmptyResultDataAccessException e) {
		}
		return resp;
	}

	public MasterSheetBean findEntityById(String strId) {

		String sql = MasterSheetBeanMapper.BASE_SQL + " where id = ? ";
		MasterSheetBean resp = null;
		MasterSheetBeanMapper mapper = new MasterSheetBeanMapper();
		Object[] params = new Object[] { strId };
		try {
			resp = this.getJdbcTemplate().queryForObject(sql, mapper, params);
		} catch (EmptyResultDataAccessException e) {
		}
		return resp;
	}

	public MasterSheetBean findEntityByName(String strName) {

		String sql = MasterSheetBeanMapper.BASE_SQL + " where masterSheetNo = ? ";
		MasterSheetBean resp = null;
		MasterSheetBeanMapper mapper = new MasterSheetBeanMapper();
		Object[] params = new Object[] { strName };
		try {
			resp = this.getJdbcTemplate().queryForObject(sql, mapper, params);
		} catch (EmptyResultDataAccessException e) {
		}
		return resp;
	}

	public int addEntity(MasterSheetBean entity) {

		String sql = "Insert into masterdatasheet(masterSheetNo, invoiceNo, invoiceDate, buyersPONo, buyersPODate, "
				+ "PINo, PIDate, customerFullName, address1, address2, city, country, GSTNo, IECNo, emailId, incoTerms, "
				+ "POL, POD, finalDestination, countryOfOrigin, currency, totalAmount, freight, otherCharges, "
				+ "ourBankDetails, paymentTerms, additionalConditions, partialDelivery, transShipment, legalisation, "
				+ "tolerance, latestShipmentDate, LCExpiryPlace, documentsProvided, DNNo, deliveryDate, LCNo, LPONo, "
				+ "HSCode, grossWeight, netWeight, grossWeightUnit, netWeightUnit, manufacturerProducer, LCNo_1, "
				+ "LCDate, LCOpeningBank, LCExpiryDate, LCApplicant, DraftsToBeDrawnOn, PLNo, PLDate, BINNo, "
				+ "applicantTINNo, bankTINNo, BOENo, BOEDate, amount, BLNo, BLDate, shippingLine, vesselName, "
				+ "ETDDate, ETADate, insurancePolicyNo, coverNoteNo, otherConditions1, otherConditions2, "
				+ "otherConditions3, otherConditions4)"
				+ " values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,"
				+ "?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
		int resp = 0;
		Object[] params = new Object[] { 
				entity.getMasterSheetNo(), 
				entity.getInvoiceNo(),
				entity.getInvoiceDate(),
				entity.getBuyersPONo(),
				entity.getBuyersPODate(),
				entity.getPINo(),
				entity.getPIDate(),
				entity.getCustomerFullName(),
				entity.getAddress1(),
				entity.getAddress2(),
				entity.getCity(),
				entity.getCountry(),
				entity.getGSTNo(),
				entity.getIECNo(),
				entity.getEmailId(),
				entity.getIncoTerms(),
				entity.getPOL(),
				entity.getPOD(),
				entity.getFinalDestination(),
				entity.getCountryOfOrigin(),
				entity.getCurrency(),
				entity.getTotalAmount(),
				entity.getFreight(),
				entity.getOtherCharges(),
				entity.getOurBankDetails(),
				entity.getPaymentTerms(),
				entity.getAdditionalConditions(),
				entity.getPartialDelivery(),
				entity.getTransShipment(),
				entity.getLegalisation(),
				entity.getTolerance(),
				entity.getLatestShipmentDate(),
				entity.getLCExpiryPlace(),
				entity.getDocumentsProvided(),
				entity.getDNNo(),
				entity.getDeliveryDate(),
				entity.getLCNo(),
				entity.getLPONo(),
				entity.getHSCode(),
				entity.getGrossWeight(),
				entity.getNetWeight(),
				entity.getGrossWeightUnit(),
				entity.getNetWeightUnit(),
				entity.getManufacturerProducer(),
				entity.getLCNo_1(),
				entity.getLCDate(),
				entity.getLCOpeningBank(),
				entity.getLCExpiryDate(),
				entity.getLCApplicant(),
				entity.getDraftsToBeDrawnOn(),
				entity.getPLNo(),
				entity.getPLDate(),
				entity.getBINNo(),
				entity.getApplicantTINNo(),
				entity.getBankTINNo(),
				entity.getBOENo(),
				entity.getBOEDate(),
				entity.getAmount(),
				entity.getBLNo(),
				entity.getBLDate(),
				entity.getShippingLine(),
				entity.getVesselName(),
				entity.getETDDate(),
				entity.getETADate(),
				entity.getInsurancePolicyNo(),
				entity.getCoverNoteNo(),
				entity.getOtherConditions1(),
				entity.getOtherConditions2(),
				entity.getOtherConditions3(),
				entity.getOtherConditions4()
		};
		try {
			resp = this.getJdbcTemplate().update(sql, params);
		} catch (EmptyResultDataAccessException e) {
		}
		return resp;
	}

	public int deleteEntity(MasterSheetBean entity) {

		String sql = "delete from masterdatasheet where id = ?;";
		int resp = 0;
		Object[] params = new Object[] { entity.getId() };
		try {
			resp = this.getJdbcTemplate().update(sql, params);
		} catch (EmptyResultDataAccessException e) {
		}
		return resp;
	}

	public int updateEntity(MasterSheetBean entity) {

		String sql = "update masterdatasheet set "
				+ "masterSheetNo=?, "
				+ "invoiceNo=?, "
				+ "invoiceDate=?, "
				+ "buyersPONo=?, "
				+ "buyersPODate=?, "
				+ "PINo=?, "
				+ "PIDate=?, "
				+ "customerFullName=?, "
				+ "address1=?, "
				+ "address2=?, "
				+ "city=?, "
				+ "country=?, "
				+ "GSTNo=?, "
				+ "IECNo=?, "
				+ "emailId=?, "
				+ "incoTerms=?, "
				+ "POL=?, "
				+ "POD=?, "
				+ "finalDestination=?, "
				+ "countryOfOrigin=?, "
				+ "currency=?, "
				+ "totalAmount=?, "
				+ "freight=?, "
				+ "otherCharges=?, "
				+ "ourBankDetails=?, "
				+ "paymentTerms=?, "
				+ "additionalConditions=?, "
				+ "partialDelivery=?, "
				+ "transShipment=?, "
				+ "legalisation=?, "
				+ "tolerance=?, "
				+ "latestShipmentDate=?, "
				+ "LCExpiryPlace=?, "
				+ "documentsProvided=?, "
				+ "DNNo=?, "
				+ "deliveryDate=?, "
				+ "LCNo=?, "
				+ "LPONo=?, "
				+ "HSCode=?, "
				+ "grossWeight=?, "
				+ "netWeight=?, "
				+ "grossWeightUnit=?, "
				+ "netWeightUnit=?, "
				+ "manufacturerProducer=?, "
				+ "LCNo_1=?, "
				+ "LCDate=?, "
				+ "LCOpeningBank=?, "
				+ "LCExpiryDate=?, "
				+ "LCApplicant=?, "
				+ "DraftsToBeDrawnOn=?, "
				+ "PLNo=?, "
				+ "PLDate=?, "
				+ "BINNo=?, "
				+ "applicantTINNo=?, "
				+ "bankTINNo=?, "
				+ "BOENo=?, "
				+ "BOEDate=?, "
				+ "amount=?, "
				+ "BLNo=?, "
				+ "BLDate=?, "
				+ "shippingLine=?, "
				+ "vesselName=?, "
				+ "ETDDate=?, "
				+ "ETADate=?, "
				+ "insurancePolicyNo=?, "
				+ "coverNoteNo=?, "
				+ "otherConditions1=?, "
				+ "otherConditions2=?, "
				+ "otherConditions3=?, "
				+ "otherConditions4=? "
				+ "WHERE id=?";
		
		int resp = 0;
		Object[] params = new Object[] { 				
				entity.getMasterSheetNo(), 
				entity.getInvoiceNo(),
				entity.getInvoiceDate(),
				entity.getBuyersPONo(),
				entity.getBuyersPODate(),
				entity.getPINo(),
				entity.getPIDate(),
				entity.getCustomerFullName(),
				entity.getAddress1(),
				entity.getAddress2(),
				entity.getCity(),
				entity.getCountry(),
				entity.getGSTNo(),
				entity.getIECNo(),
				entity.getEmailId(),
				entity.getIncoTerms(),
				entity.getPOL(),
				entity.getPOD(),
				entity.getFinalDestination(),
				entity.getCountryOfOrigin(),
				entity.getCurrency(),
				entity.getTotalAmount(),
				entity.getFreight(),
				entity.getOtherCharges(),
				entity.getOurBankDetails(),
				entity.getPaymentTerms(),
				entity.getAdditionalConditions(),
				entity.getPartialDelivery(),
				entity.getTransShipment(),
				entity.getLegalisation(),
				entity.getTolerance(),
				entity.getLatestShipmentDate(),
				entity.getLCExpiryPlace(),
				entity.getDocumentsProvided(),
				entity.getDNNo(),
				entity.getDeliveryDate(),
				entity.getLCNo(),
				entity.getLPONo(),
				entity.getHSCode(),
				entity.getGrossWeight(),
				entity.getNetWeight(),
				entity.getGrossWeightUnit(),
				entity.getNetWeightUnit(),
				entity.getManufacturerProducer(),
				entity.getLCNo_1(),
				entity.getLCDate(),
				entity.getLCOpeningBank(),
				entity.getLCExpiryDate(),
				entity.getLCApplicant(),
				entity.getDraftsToBeDrawnOn(),
				entity.getPLNo(),
				entity.getPLDate(),
				entity.getBINNo(),
				entity.getApplicantTINNo(),
				entity.getBankTINNo(),
				entity.getBOENo(),
				entity.getBOEDate(),
				entity.getAmount(),
				entity.getBLNo(),
				entity.getBLDate(),
				entity.getShippingLine(),
				entity.getVesselName(),
				entity.getETDDate(),
				entity.getETADate(),
				entity.getInsurancePolicyNo(),
				entity.getCoverNoteNo(),
				entity.getOtherConditions1(),
				entity.getOtherConditions2(),
				entity.getOtherConditions3(),
				entity.getOtherConditions4(),
				entity.getId()
		};
		
		try {
			resp = this.getJdbcTemplate().update(sql, params);
		} catch (EmptyResultDataAccessException e) {
		}
		return resp;
	}

}