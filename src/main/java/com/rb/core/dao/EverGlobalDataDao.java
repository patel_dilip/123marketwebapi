package com.rb.core.dao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class EverGlobalDataDao extends JdbcDaoSupport {

	private static final Logger logger = LoggerFactory.getLogger(EverGlobalDataDao.class);
	
	@Autowired
	public EverGlobalDataDao(DataSource dataSource) {
		this.setDataSource(dataSource);
	}

	public Map<String, Object> getCountry() {
		Map<String, Object> resp = null;
		logger.info("Calling Database Procedure usp_GetCountry ...");
		try {
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(this.getJdbcTemplate()).withProcedureName("usp_GetCountry");
			Map<String, Object> inParamMap = new HashMap<String, Object>();
			SqlParameterSource in = new MapSqlParameterSource(inParamMap);
			resp = simpleJdbcCall.execute(in);
		} catch (EmptyResultDataAccessException ex) {
			logger.error("Error in getCountry..." + ex.getMessage());
		}
		return resp;
	}
	
	public Map<String, Object> getCountryCode() {
		Map<String, Object> resp = null;
		logger.info("Calling Database Procedure usp_GetCountryCode ...");		
		try {
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(this.getJdbcTemplate()).withProcedureName("usp_GetCountryCode");
			Map<String, Object> inParamMap = new HashMap<String, Object>();
			SqlParameterSource in = new MapSqlParameterSource(inParamMap);
			resp = simpleJdbcCall.execute(in);
		} catch (EmptyResultDataAccessException ex) {
			logger.error("Error in getCountryCode..." + ex.getMessage());			
		}
		return resp;
	}	

	public Map<String, Object> getValidUser(String vchUserID, String nvrPassword){
		Map<String, Object> resp = null;
		logger.info("Calling Database Procedure usp_UserValidate ...");		
		try {
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(this.getJdbcTemplate()).withProcedureName("usp_UserValidate");
			Map<String, Object> inParamMap = new HashMap<String, Object>();
			inParamMap.put("UserId", vchUserID);
			inParamMap.put("Password", nvrPassword);
			SqlParameterSource in = new MapSqlParameterSource(inParamMap);
			resp = simpleJdbcCall.execute(in);
		} catch (EmptyResultDataAccessException ex) {
			logger.error("Error in getValidUser..." + ex.getMessage());			
		}
		return resp;
	}		
	
	public List<Map<String, Object>> getLatestLeads(Map<String,Object> filterData) {
		List<Map<String, Object>> resp = null;
		logger.info("Calling Database getLatestLeads ...");
		try {
			String strSqlStart = "SELECT "
					+ "tblproductdetails.intProductId, "
					+ "tblproductdetails.vchUserType, "
					+ "tblproductdetails.vchTitle, "
					+ "tblproductdetails.vchCategory, "
					+ "tblproductdetails.vchDescriptions, "
					+ "CONCAT('Uploads/',tblproductdetails.vchFileName) AS ImgPath, "
					+ "tbluserdetails.vchCompanyName, "
					+ "tbluserdetails.vchAddress, "
					+ "vchUserID, "
					+ "tbluserdetails.vchZipCode, "
					+ "tbluserdetails.vchCity, "
					+ "tbluserdetails.vchCountry, "
					+ "tbluserdetails.vchContactPerson, "
					+ "(tbluserdetails.vchCountryCallCode + '' + tbluserdetails.vchMobileNo) AS vchMobileNo,"
					+ "(tbluserdetails.vchWhatsAppCountryCode + '' + tbluserdetails.vchWhatsAppNo) AS vchWhatsAppNo, "
					+ "(tbluserdetails.vchTelePhoneCountryCode + '' + tbluserdetails.vchTelePhoneNo) AS vchTelePhoneNo, "
					+ "(tbluserdetails.vchWeChatCountryCode + '' + tbluserdetails.vchWeChatNo) AS vchWeChatNo "
					+ "FROM tblproductdetails "
					+ "INNER JOIN tbluserdetails ON tblproductdetails.intCreatedBy = tbluserdetails.intUserID "
					+ "AND tblproductdetails.IsActive = 1 "
					+ "AND tbluserdetails.IsActive = 1 ";
			
			String strSqlFilter = getFilterSql(filterData);
			
			String strSqlEnd = "ORDER BY tblproductdetails.dtCreatedDate DESC;";
			
			resp = this.getJdbcTemplate().queryForList(strSqlStart+strSqlFilter+strSqlEnd);

		} catch (EmptyResultDataAccessException ex) {
			logger.error("Error in getLatestLeads..." + ex.getMessage());
		}
		return resp;
	}
	
	private String getFilterSql(Map<String, Object> filterData) {
		String strFilterSql = "";
		if(null != filterData && !filterData.isEmpty()) {
			if(null != filterData.get("postType") && !filterData.get("postType").toString().isEmpty()) {
				if(!filterData.get("postType").toString().equals("seller,buyer")) {
					strFilterSql += " AND tblproductdetails.vchUserType = '"+filterData.get("postType").toString()+"' ";
				}
			}
			if(null != filterData.get("category") && !filterData.get("category").toString().isEmpty()) {
				strFilterSql += " AND tblproductdetails.vchCategory = '"+filterData.get("category").toString()+"' ";
			}
			if(null != filterData.get("keyword") && !filterData.get("keyword").toString().isEmpty()) {
				//strFilterSql += " AND (tblproductdetails.vchCategory like '%"+filterData.get("keyword").toString()+"%' "
						//+ "OR tblproductdetails.vchUserType like '%"+filterData.get("keyword").toString()+"%' "
						//+ "OR tblproductdetails.vchTitle like '%"+filterData.get("keyword").toString()+"%') ";
						//+ "OR tblproductdetails.vchDescriptions like '%"+filterData.get("keyword").toString()+"%') ";
				strFilterSql += " AND (tblproductdetails.vchTitle like '%"+filterData.get("keyword").toString()+"%') ";
			}
			if(null != filterData.get("country") && !filterData.get("country").toString().isEmpty()) {
				strFilterSql += " AND tbluserdetails.vchCountry = '"+filterData.get("country").toString()+"' ";
			}
			
		}
		return strFilterSql;
	}

	public Map<String, Object> getItemDetails(String itemId) {
		Map<String, Object> resp = null;
		logger.info("Calling Database getItemDetails for itemId ..."+itemId);
		try {
			String strSQL = "SELECT "
				    +" tblproductdetails.intProductId AS itemId,"
				    +" tblproductdetails.vchUserType AS itemType,"
				    +" tblproductdetails.vchTitle AS itemName,"
				    +" tblproductdetails.vchCategory AS itemCategory,"
				    +" tblproductdetails.vchDescriptions AS itemDescription,"
				    +" CONCAT('Uploads/',tblproductdetails.vchFileName) AS itemImg,"
				    +" tbluserdetails.vchUserID AS userId,"
				    +" tbluserdetails.vchUserType AS userType,"
				    +" CONCAT('Uploads/', tbluserdetails.vchFileName) AS userImg,"
				    +" tbluserdetails.vchCompanyName AS userName,"
				    +" CONCAT(IF(tbluserdetails.vchAddress IS NULL OR tbluserdetails.vchAddress = '', '', CONCAT(tbluserdetails.vchAddress, ', ')),"
				    +" IF(tbluserdetails.vchCountry IS NULL OR tbluserdetails.vchCountry = '', '', CONCAT(tbluserdetails.vchCountry, ', ')),"
				    +" IF(tbluserdetails.vchCity IS NULL OR tbluserdetails.vchCity = '', '', CONCAT(tbluserdetails.vchCity, ', '))) AS userAddress,"
				    +" upper(vchUserID) AS userEmail,"
				    +" date_format(tblproductdetails.dtCreatedDate,'%d-%m-%Y') AS userPostedOn,"
				    +" CONCAT(tbluserdetails.vchCountryCallCode,tbluserdetails.vchMobileNo) AS userPhoneWA,"
				    +" CONCAT(tbluserdetails.vchCountryCallCode,tbluserdetails.vchMobileNo) AS userPhone"
				+" FROM tblproductdetails INNER JOIN tbluserdetails ON tblproductdetails.intCreatedBy = tbluserdetails.intUserID"
				    +" AND tblproductdetails.IsActive = 1"
				    +" AND tbluserdetails.IsActive = 1"
				    +" AND tblproductdetails.intProductId = "+itemId;
			
			resp = this.getJdbcTemplate().queryForMap(strSQL);

		} catch (EmptyResultDataAccessException ex) {
			logger.error("Error in getItemDetails..." + ex.getMessage());
		}
		return resp;
	}	
	
	public Map<String, Object> userInfoAndLeads(String userId) {
		Map<String, Object> resp = null;
		logger.info("Calling Database getLatestLeads...");
		try {
			String strSql = "Select	vchUserName,vchUserType,dtCreatedDate,vchFileName from tbluserdetails where intUserID ='"+userId+"'";		
			resp = this.getJdbcTemplate().queryForMap(strSql);

		} catch (EmptyResultDataAccessException ex) {
			logger.error("Error in getLatestLeads..." + ex.getMessage());
		}
		return resp;
	}

	public List<Map<String, Object>> leadDetails(String userId) {
		// TODO Auto-generated method stub
		List<Map<String,Object>> resp = new ArrayList<Map<String,Object>>();
		String sql = "Select intProductId, vchUserType, vchFileName, vchDescriptions from tblproductdetails where intCreatedBy= ? and IsActive=1 ";
		
		Object[] params = new Object[] { userId };
		try {
			resp = this.getJdbcTemplate().queryForList(sql,params);
		} catch (EmptyResultDataAccessException ex) {
			ex.getMessage();
		}
		return resp;
	}

	public List<Map<String, Object>> getCountryList() {
		List<Map<String,Object>> resp = null;
		logger.info("Calling Database getCountryList  ...");
		try {
			String strSql = "SELECT intCountryId AS id, vchCountryName AS name FROM tblcountry;";		
			resp = this.getJdbcTemplate().queryForList(strSql);

		} catch (EmptyResultDataAccessException ex) {
			logger.error("Error in getCountryList..." + ex.getMessage());
		}
		return resp;
	}
	
	public List<Map<String, Object>> getStateList() {
		List<Map<String,Object>> resp = null;
		logger.info("Calling Database getStateList  ...");
		try {
			String strSql = "select	id, name from tbl_states;";		
			resp = this.getJdbcTemplate().queryForList(strSql);

		} catch (EmptyResultDataAccessException ex) {
			logger.error("Error in getStateList..." + ex.getMessage());
		}
		return resp;
	}
	
	public List<Map<String, Object>> getCityList() {
		List<Map<String,Object>> resp = null;
		logger.info("Calling Database getCityList  ...");
		try {
			String strSql = "select	id, name from tbl_cities;";		
			resp = this.getJdbcTemplate().queryForList(strSql);

		} catch (EmptyResultDataAccessException ex) {
			logger.error("Error in getCityList..." + ex.getMessage());
		}
		return resp;
	}
	
	public List<Map<String, Object>> getPhoneCC() {
		List<Map<String,Object>> resp = null;
		logger.info("Calling Database getPhoneCC  ...");
		try {
			String strSql = "SELECT vchCountryName AS country, vchCallCode AS phoneCC FROM tblcountry;";		
			resp = this.getJdbcTemplate().queryForList(strSql);

		} catch (EmptyResultDataAccessException ex) {
			logger.error("Error in getPhoneCC..." + ex.getMessage());
		}
		return resp;
	}

	public String addUserProfile(Map<String, Object> userData) {

		String sql = "INSERT INTO tbluserdetails ("
				+ "vchUserType, "
				+ "vchUserID, "
				+ "nvrPassword, "
				+ "vchCompanyName, "
				+ "vchProduct, "
				+ "vchAddress, "
				+ "vchZipCode, "
				+ "vchCity, "
				+ "vchCountry, "
				+ "vchContactPerson, "
				+ "vchCountryCallCode, "
				+ "vchMobileNo, "
				+ "vchWhatsAppCountryCode, "
				+ "vchWhatsAppNo, "
				+ "vchTelePhoneCountryCode, "
				+ "vchTelePhoneNo, "
				+ "vchWeChatCountryCode, "
				+ "vchWeChatNo, "
				+ "vchFileName, "
				+ "IsActive, "
				+ "dtCreatedDate, "
				+ "intCreatedBy, "
				+ "vchIPAddress, "
				+ "intModifiedBy, "
				+ "dtModifiedBy, "
				+ "vchState"
				+ ") VALUES (?,?,SHA1(?),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,sysdate(),?,?,?,sysdate(),?);";
		int resp = 0;
		
		
		Object[] params = new Object[] { 
				userData.get("userType"), 
				userData.get("username"), 
				userData.get("password"), 
				userData.get("companyName"), 
				userData.get("products"), 
				userData.get("address"), 
				userData.get("pincode"), 
				userData.get("city"), 
				userData.get("country"), 
				userData.get("contactPerson"), 
				userData.get("phoneCCM"), 
				userData.get("mobileNo"), 
				userData.get("phoneCCWA"), 
				userData.get("whatsAppNo"), 
				userData.get("phoneCCT"), 
				userData.get("telephoneNo"), 
				userData.get("phoneCCWC"), 
				userData.get("weChatNo"), 
				userData.get("imgPath"), 
				1, 
				0, 
				"0.0.0.0", 
				0, 
				userData.get("state")
		};
		
		try {
			resp = this.getJdbcTemplate().update(sql, params);
		} catch (EmptyResultDataAccessException e) {
		}
		return String.valueOf(resp);
	}
	
	public String updateUserProfile(Map<String, Object> userData) {

		String sql = "UPDATE tbluserdetails SET "
				+ "vchUserType=?, "
				+ "vchCompanyName=?, "
				+ "vchProduct=?, "
				+ "vchAddress=?, "
				+ "vchZipCode=?, "
				+ "vchCity=?, "
				+ "vchCountry=?, "
				+ "vchContactPerson=?, "
				+ "vchCountryCallCode=?, "
				+ "vchMobileNo=?, "
				+ "vchWhatsAppCountryCode=?, "
				+ "vchWhatsAppNo=?, "
				+ "vchTelePhoneCountryCode=?, "
				+ "vchTelePhoneNo=?, "
				+ "vchWeChatCountryCode=?, "
				+ "vchWeChatNo=?, "
				+ "dtModifiedBy=sysdate(), ";
			Object imgPath = userData.get("imgPath"); 
			if(null != imgPath && !imgPath.toString().isEmpty())
				sql += "vchFileName=?, ";
			sql += "vchState=? "
				+ "WHERE vchUserID=? ";

		int resp = 0;
	
		Object[] params = new Object[] { 
				userData.get("userType"), 
				userData.get("companyName"), 
				userData.get("products"), 
				userData.get("address"), 
				userData.get("pincode"), 
				userData.get("city"), 
				userData.get("country"), 
				userData.get("contactPerson"), 
				userData.get("phoneCCM"), 
				userData.get("mobileNo"), 
				userData.get("phoneCCWA"), 
				userData.get("whatsAppNo"), 
				userData.get("phoneCCT"), 
				userData.get("telephoneNo"), 
				userData.get("phoneCCWC"), 
				userData.get("weChatNo")
		};
		if(null != imgPath && !imgPath.toString().isEmpty())
			params = appendValue(params, userData.get("imgPath"));
		params = appendValue(params, userData.get("state"));
		params = appendValue(params, userData.get("username"));
		
		try {
			resp = this.getJdbcTemplate().update(sql, params);
		} catch (EmptyResultDataAccessException e) {
		}
		return String.valueOf(resp);
	}

	public String validateUserInfo(Map<String, Object> userData) {
		String strPass = null;
		String strUser = null;
		String resp = null;
		String strResp = "";
		strUser = userData.get("username").toString();
		strPass = userData.get("password").toString();
		String strSQL = "SELECT intUserID FROM tbluserdetails where vchUserID = '"+strUser+"' and nvrPassword = SHA1('"+strPass+"');";
		try {
			resp = this.getJdbcTemplate().queryForObject(strSQL,String.class);
		} catch (EmptyResultDataAccessException e) {}		
		if(null!=resp && !resp.isEmpty())
			strResp = resp;
		return strResp;
	}
	
	public Map<String, Object> validateUserInfoN(Map<String, Object> userData) {
		String strPass = null;
		String strUser = null;
		Map<String, Object> resp = null;
		strUser = userData.get("username").toString();
		strPass = userData.get("password").toString();
		String strSQL = "SELECT intUserID as userId, vchUserType as userType, vchUserID as username, vchCountry as userCountry FROM tbluserdetails where vchUserID = '"+strUser+"' and nvrPassword = SHA1('"+strPass+"');";
		try {
			resp = this.getJdbcTemplate().queryForMap(strSQL);
		} catch (EmptyResultDataAccessException e) {}		
		return resp;
	}	

	public Map<String, Object> getUserData(String userId) {
		Map<String, Object> resp = null;
		logger.info("Calling Database getUserData...");
		try {
			String strSql = "SELECT intUserID, vchUserType as userType, vchUserID as username, "
					+ "vchCompanyName as companyName, vchProduct as products, vchAddress as address, "
					+ "vchZipCode as pincode, vchCity as city, vchCountry as country, "
					+ "vchContactPerson as contactPerson, vchCountryCallCode as phoneCCM, "
					+ "vchMobileNo as mobileNo, vchWhatsAppCountryCode as phoneCCWA, "
					+ "vchWhatsAppNo as whatsAppNo, vchTelePhoneCountryCode as phoneCCT, "
					+ "vchTelePhoneNo as telephoneNo, vchWeChatCountryCode as phoneCCWC, "
					+ "vchWeChatNo as weChatNo, vchFileName as imgPath, IsActive, "
					+ "dtCreatedDate, intCreatedBy, vchIPAddress, intModifiedBy, dtModifiedBy, "
					+ "vchState as state FROM tbluserdetails "
					+ "where vchUserID ='"+userId+"'";		
			
			resp = this.getJdbcTemplate().queryForMap(strSql);

		} catch (EmptyResultDataAccessException ex) {
			logger.error("Error in getUserData..." + ex.getMessage());
		}
		return resp;
	}		
	
	  private Object[] appendValue(Object[] obj, Object newObj) {

		    ArrayList<Object> temp = new ArrayList<Object>(Arrays.asList(obj));
		    temp.add(newObj);
		    return temp.toArray();

		  }

	public List<Map<String, Object>> getCategoryList() {
		List<Map<String,Object>> resp = null;
		logger.info("Calling Database getCategoryList  ...");
		try {
			String strSql = "SELECT distinct vchCategory AS id, vchCategory AS name FROM tblproductdetails where vchCategory is not null;";		
			resp = this.getJdbcTemplate().queryForList(strSql);

		} catch (EmptyResultDataAccessException ex) {
			logger.error("Error in getCategoryList..." + ex.getMessage());
		}
		return resp;
	}

	public Map<String, Object> getPostData(String postId) {
		Map<String, Object> resp = null;
		logger.info("Calling Database getPostData...");
		try {
			String strSql = "SELECT intProductId as postId, vchTitle as postTitle, vchUserType as postType, "
					+ "vchCategory as postCategory, vchDescriptions as postDetails, vchFileName as imgPath, "
					+ "intCreatedBy as postUser FROM tblproductdetails "
					+ "where intProductId ='"+postId+"' and IsActive=1 ";		
			
			resp = this.getJdbcTemplate().queryForMap(strSql);

		} catch (EmptyResultDataAccessException ex) {
			logger.error("Error in getPostData..." + ex.getMessage());
		}
		return resp;
	}

	public String addPostData(Map<String, Object> postData) {

		String sql = "INSERT INTO tblproductdetails ("
				+ "vchUserType, "
				+ "vchTitle, "
				+ "vchCategory, "
				+ "vchDescriptions, "
				+ "vchFileName, "
				+ "IsActive, "
				+ "dtCreatedDate, "
				+ "intCreatedBy "
				+ ") VALUES (?,?,?,?,?,?,sysdate(),?);";
		int resp = 0;
		
		
		Object[] params = new Object[] { 
				postData.get("postType"), 
				postData.get("postTitle"), 
				postData.get("postCategory"), 
				postData.get("postDetails"), 
				postData.get("imgPath"),
				1, 
				postData.get("postUser")
		};
		
		try {
			resp = this.getJdbcTemplate().update(sql, params);
		} catch (EmptyResultDataAccessException e) {
		}
		return String.valueOf(resp);
	}

	public String updatePostData(Map<String, Object> postData) {

		String sql = "UPDATE tblproductdetails SET "
				+ "vchUserType=?, "
				+ "vchTitle=?, "
				+ "vchCategory=?, "
				+ "vchDescriptions=?, ";
			Object imgPath = postData.get("imgPath"); 
			if(null != imgPath && !imgPath.toString().isEmpty())
				sql += "vchFileName=?, ";
			sql += "dtModifiedBy=sysdate() "
				+ "WHERE intProductId=? ";

		int resp = 0;
	
		Object[] params = new Object[] { 
				postData.get("postType"), 
				postData.get("postTitle"), 
				postData.get("postCategory"), 
				postData.get("postDetails")
		};
		
		if(null != imgPath && !imgPath.toString().isEmpty())
			params = appendValue(params, postData.get("imgPath"));
		
		params = appendValue(params, postData.get("postId"));
		
		try {
			resp = this.getJdbcTemplate().update(sql, params);
		} catch (EmptyResultDataAccessException e) {
		}
		return String.valueOf(resp);
	}

	public String deletePostData(String postId) {
		String sql = "update tblproductdetails set ISActive = 0 where intProductId = ? ";
		int resp = 0;
		Object[] params = new Object[] {postId};
		try {
			resp = this.getJdbcTemplate().update(sql, params);
		} catch (EmptyResultDataAccessException e) {
		}
		return String.valueOf(resp);
	}
	
}