package com.rb.core.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.springframework.jdbc.core.RowMapper;

import com.rb.core.model.MasterSheetBean;

public class MasterSheetBeanMapper implements RowMapper<MasterSheetBean> {

	public static final String BASE_SQL //
	= "select id," + 
			"	masterSheetNo, " + 
			"	invoiceNo, " + 
			"	invoiceDate," + 
			"	buyersPONo, " + 
			"	buyersPODate, " + 
			"	PINo, " + 
			"	PIDate, " + 
			"	customerFullName, " + 
			"	address1, " + 
			"	address2, " + 
			"	city, " + 
			"	country, " + 
			"	GSTNo, " + 
			"	IECNo, " + 
			"	emailId, " + 
			"	incoTerms, " + 
			"	POL, " + 
			"	POD, " + 
			"	finalDestination, " + 
			"	countryOfOrigin, " + 
			"	currency, " + 
			"	totalAmount, " + 
			"	freight, " + 
			"	otherCharges, " + 
			"	ourBankDetails, " + 
			"	paymentTerms, " + 
			"	additionalConditions, " + 
			"	partialDelivery, " + 
			"	transShipment, " + 
			"	legalisation, " + 
			"	tolerance, " + 
			"	latestShipmentDate, " + 
			"	LCExpiryPlace, " + 
			"	documentsProvided, " + 
			"	DNNo, " + 
			"	deliveryDate, " + 
			"	LCNo, " + 
			"	LPONo, " + 
			"	HSCode, " + 
			"	grossWeight, " + 
			"	netWeight, " + 
			"	grossWeightUnit, " + 
			"	netWeightUnit, " + 
			"	manufacturerProducer, " + 
			"	LCNo_1, " + 
			"	LCDate, " + 
			"	LCOpeningBank, " + 
			"	LCExpiryDate, " + 
			"	LCApplicant, " + 
			"	DraftsToBeDrawnOn, " + 
			"	PLNo, " + 
			"	PLDate, " + 
			"	BINNo, " + 
			"	applicantTINNo, " + 
			"	bankTINNo, " + 
			"	BOENo, " + 
			"	BOEDate, " + 
			"	amount, " + 
			"	BLNo, " + 
			"	BLDate, " + 
			"	shippingLine, " + 
			"	vesselName, " + 
			"	ETDDate, " + 
			"	ETADate, " + 
			"	insurancePolicyNo, " + 
			"	coverNoteNo, " + 
			"	otherConditions1, " + 
			"	otherConditions2, " + 
			"	otherConditions3, " + 
			"	otherConditions4 " + 
			"from " + 
			"	masterdatasheet";

	@Override
	public MasterSheetBean mapRow(ResultSet rs, int rowNum) throws SQLException {
	
		long id = rs.getLong("id");
		String masterSheetNo = rs.getString("masterSheetNo");
		String invoiceNo = rs.getString("invoiceNo");
		String buyersPONo = rs.getString("buyersPONo");
		String PINo = rs.getString("PINo");
		String customerFullName = rs.getString("customerFullName");
		String address1 = rs.getString("address1");
		String address2 = rs.getString("address2");
		String city = rs.getString("city");
		String country = rs.getString("country");
		String GSTNo = rs.getString("GSTNo");
		String IECNo = rs.getString("IECNo");
		String emailId = rs.getString("emailId");
		String incoTerms = rs.getString("incoTerms");
		String POL = rs.getString("POL");
		String POD = rs.getString("POD");
		String finalDestination = rs.getString("finalDestination");
		String countryOfOrigin = rs.getString("countryOfOrigin");
		String currency = rs.getString("currency");
		String ourBankDetails = rs.getString("ourBankDetails");
		String paymentTerms = rs.getString("paymentTerms");
		String additionalConditions = rs.getString("additionalConditions");
		String partialDelivery = rs.getString("partialDelivery");
		String transShipment = rs.getString("transShipment");
		String legalisation = rs.getString("legalisation");
		String LCExpiryPlace = rs.getString("LCExpiryPlace");
		String documentsProvided = rs.getString("documentsProvided");
		String DNNo = rs.getString("DNNo");
		String LCNo = rs.getString("LCNo");
		String LPONo = rs.getString("LPONo");
		String HSCode = rs.getString("HSCode");
		String grossWeightUnit = rs.getString("grossWeightUnit");
		String netWeightUnit = rs.getString("netWeightUnit");
		String manufacturerProducer = rs.getString("manufacturerProducer");
		String LCNo_1 = rs.getString("LCNo_1");
		String LCOpeningBank = rs.getString("LCOpeningBank");
		String LCApplicant = rs.getString("LCApplicant");
		String DraftsToBeDrawnOn = rs.getString("DraftsToBeDrawnOn");
		String PLNo = rs.getString("PLNo");
		String BINNo = rs.getString("BINNo");
		String applicantTINNo = rs.getString("applicantTINNo");
		String bankTINNo = rs.getString("bankTINNo");
		String BOENo = rs.getString("BOENo");
		String BLNo = rs.getString("BLNo");
		String shippingLine = rs.getString("shippingLine");
		String vesselName = rs.getString("vesselName");
		String insurancePolicyNo = rs.getString("insurancePolicyNo");
		String coverNoteNo = rs.getString("coverNoteNo");
		String otherConditions1 = rs.getString("otherConditions1");
		String otherConditions2 = rs.getString("otherConditions2");
		String otherConditions3 = rs.getString("otherConditions3");
		String otherConditions4 = rs.getString("otherConditions4");
		Date invoiceDate = rs.getDate("invoiceDate");
		Date buyersPODate = rs.getDate("buyersPODate");
		Date PIDate = rs.getDate("PIDate");
		Date latestShipmentDate = rs.getDate("latestShipmentDate");
		Date deliveryDate = rs.getDate("deliveryDate");
		Date LCDate = rs.getDate("LCDate");
		Date LCExpiryDate = rs.getDate("LCExpiryDate");
		Date PLDate = rs.getDate("PLDate");
		Date BOEDate = rs.getDate("BOEDate");
		Date BLDate = rs.getDate("BLDate");
		Date ETDDate = rs.getDate("ETDDate");
		Date ETADate = rs.getDate("ETADate");
		Double totalAmount = rs.getDouble("totalAmount");
		Double freight = rs.getDouble("freight");
		Double otherCharges = rs.getDouble("otherCharges");
		Double amount = rs.getDouble("amount");
		int tolerance = rs.getInt("tolerance");
		int grossWeight = rs.getInt("grossWeight");
		int netWeight = rs.getInt("netWeight");
	
		return new MasterSheetBean(id, masterSheetNo, invoiceNo, buyersPONo, PINo, customerFullName,
				address1, address2, city, country, GSTNo, IECNo, emailId, incoTerms, POL, POD, 
				finalDestination, countryOfOrigin, currency, ourBankDetails, paymentTerms, additionalConditions,
				partialDelivery, transShipment, legalisation, LCExpiryPlace, documentsProvided, DNNo, LCNo, 
				LPONo, HSCode, grossWeightUnit, netWeightUnit, manufacturerProducer, LCNo_1, LCOpeningBank, 
				LCApplicant, DraftsToBeDrawnOn, PLNo, BINNo, applicantTINNo, bankTINNo, BOENo, BLNo, shippingLine,
				vesselName, insurancePolicyNo, coverNoteNo, otherConditions1, otherConditions2, otherConditions3,
				otherConditions4, invoiceDate, buyersPODate, PIDate, latestShipmentDate, deliveryDate, LCDate, 
				LCExpiryDate, PLDate, BOEDate, BLDate, ETDDate, ETADate, totalAmount, freight, otherCharges, 
				amount, tolerance, grossWeight, netWeight);
	}

}