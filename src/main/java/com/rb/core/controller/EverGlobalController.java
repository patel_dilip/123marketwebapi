package com.rb.core.controller;

import java.security.Principal;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.rb.core.dto.PostDataDto;
import com.rb.core.dto.UserDetailDataDto;
import com.rb.core.dto.UserProfileDto;
import com.rb.core.service.EverGlobalService;

@CrossOrigin(origins = {"http://localhost", "http://rbapps.cloud"})
@RestController
public class EverGlobalController {
	
	@Autowired
	EverGlobalService mainService;
	
	final static String urlPrefix = "";
	private static final Logger logger = LoggerFactory.getLogger(EverGlobalController.class);
	
    @SuppressWarnings("unchecked")
	@RequestMapping(value = urlPrefix+"/getLatestLeads", method = {RequestMethod.GET,RequestMethod.POST})
    public String getLatestLeads(@RequestParam String filterString, Model model, Principal principal) {
    	String strResponse = null;
    	Map<String,Object> filterData = null;
    	logger.info("Calling Service getLatestLeads....");
    	try {
    		filterData = new ObjectMapper().readValue(filterString, Map.class);
    	}catch(Exception ex) {
    		ex.getMessage();  
        	logger.error("Error in reading parameters for getLatestLeads...." + ex.getMessage());
    	}   	

    	List<Map<String, Object>> entityList = mainService.getLatestLeads(filterData);

    	ObjectMapper jsonMapper = new ObjectMapper();
    	jsonMapper.enable(SerializationFeature.INDENT_OUTPUT);
    	try {
    		strResponse = jsonMapper.writeValueAsString(entityList);
    	}catch(Exception ex) {
    		ex.getMessage();  
        	logger.error("Error in getLatestLeads...." + ex.getMessage());
    	}
    	logger.info("Sending Response getLatestLeads....");
    	return strResponse;
    } 
    
    @RequestMapping(value = urlPrefix+"/getItemDetails", method = {RequestMethod.GET,RequestMethod.POST})
    public String getItemDetails(@RequestParam String itemId, Model model, Principal principal) {
    	String strResponse = null;
    	
    	logger.info("Calling Service getItemDetails....");
    	Map<String, Object> entity = mainService.getItemDetails(itemId);

    	ObjectMapper jsonMapper = new ObjectMapper();
    	jsonMapper.enable(SerializationFeature.INDENT_OUTPUT);
    	try {
    		strResponse = jsonMapper.writeValueAsString(entity);
    	}catch(Exception ex) {
    		ex.getMessage();  
        	logger.error("Error in getItemDetails...." + ex.getMessage());
    	}
    	logger.info("Sending Response getItemDetails....");
    	return strResponse;
    }     
    
    @RequestMapping(value = urlPrefix+"/getUserDetailsAndLeads", method = {RequestMethod.GET,RequestMethod.POST})
    public String getUserDetailsandLeadInformation(Model model, Principal principal, String userId) {
    	String strResponse = null;
    	System.out.println("inside getUserDetailsAndLeads function"+userId);
    	
    	logger.info("Calling Service getUserDetailsAndLeads...."+userId);
    	UserDetailDataDto  entityList = mainService.getUserDetailAndLeads(userId);

    	ObjectMapper jsonMapper = new ObjectMapper();
    	jsonMapper.enable(SerializationFeature.INDENT_OUTPUT);
    	try {
    		strResponse = jsonMapper.writeValueAsString(entityList);
    	}catch(Exception ex) {
    		ex.getMessage();  
        	logger.error("Error in getLatestLeads...." + ex.getMessage());
    	}
    	logger.info("Sending Response getLatestLeads....");
    	return strResponse;
    } 
    
    @RequestMapping(value = urlPrefix+"/getUserProfileData", method = {RequestMethod.GET,RequestMethod.POST})
    public String getUserProfileData(@RequestParam String userId, Model model, Principal principal) {
    	String strResponse = null;
    	System.out.println("inside getUserProfileData function");
    	
    	logger.info("Calling Service getUserProfileData....");
    	UserProfileDto  entity = mainService.getUserProfileData(userId);

    	ObjectMapper jsonMapper = new ObjectMapper();
    	jsonMapper.enable(SerializationFeature.INDENT_OUTPUT);
    	try {
    		strResponse = jsonMapper.writeValueAsString(entity);
    	}catch(Exception ex) {
    		ex.getMessage();  
        	logger.error("Error in getUserProfileData...." + ex.getMessage());
    	}
    	logger.info("Sending Response getUserProfileData....");
    	return strResponse;
    } 
    
    @SuppressWarnings("unchecked")
	@RequestMapping(value = urlPrefix+"/updateUserProfileData", method = {RequestMethod.GET,RequestMethod.POST})
    public String updateUserProfileData(@RequestParam String dataString, @RequestParam String mode, Model model, Principal principal) {
    	String strResponse = null;
    	Map<String,Object> userData = null;
    	logger.info("Calling Service updateUserProfileData....");
    	ObjectMapper jsonMapper = new ObjectMapper();
    	jsonMapper.enable(SerializationFeature.INDENT_OUTPUT);
    	try {
    		userData = new ObjectMapper().readValue(dataString, Map.class);
    	}catch(Exception ex) {
    		ex.getMessage();  
        	logger.error("Error in updateUserProfileData...." + ex.getMessage());
    	}
    	if(mode != null && mode.equals("add"))
    		strResponse = mainService.addUserProfile(userData);
    	else
    		strResponse = mainService.updateUserProfile(userData);
    	logger.info("Sending Response updateUserProfileData....");
    	return strResponse;
    }  
    
    @SuppressWarnings("unchecked")
    @RequestMapping(value = urlPrefix+"/validateUserInfo", method = {RequestMethod.GET,RequestMethod.POST})
    public String validateUserInfo(@RequestParam String userInfo, Model model, Principal principal) {
    	String strResponse = null;
    	Map<String,Object> userData = null;
    	Map<String,Object> respData = null;
    	logger.info("Calling Service validateUserInfo....");
    	ObjectMapper jsonMapper = new ObjectMapper();
    	jsonMapper.enable(SerializationFeature.INDENT_OUTPUT);
    	try {
    		userData = new ObjectMapper().readValue(userInfo, Map.class);
    	}catch(Exception ex) {
    		ex.getMessage();  
        	logger.error("Error in validateUserInfo...." + ex.getMessage());
    	}
   		//strResponse = mainService.validateUserInfo(userData);
    	
    	respData = mainService.validateUserInfoN(userData);
    	try {
    		strResponse = jsonMapper.writeValueAsString(respData);
    	}catch(Exception ex) {
    		ex.getMessage();  
        	logger.error("Error in validateUserInfo...." + ex.getMessage());
    	}    	
    	
    	logger.info("Sending Response validateUserInfo....");
    	return strResponse;
    }   
    
    @RequestMapping(value = urlPrefix+"/getPostData", method = {RequestMethod.GET,RequestMethod.POST})
    public String getPostData(@RequestParam String postId, Model model, Principal principal) {
    	String strResponse = null;
    	System.out.println("inside getPostData function");
    	
    	logger.info("Calling Service getPostData....");
    	PostDataDto entity = mainService.getPostData(postId);

    	ObjectMapper jsonMapper = new ObjectMapper();
    	jsonMapper.enable(SerializationFeature.INDENT_OUTPUT);
    	try {
    		strResponse = jsonMapper.writeValueAsString(entity);
    	}catch(Exception ex) {
    		ex.getMessage();  
        	logger.error("Error in getPostData...." + ex.getMessage());
    	}
    	logger.info("Sending Response getPostData....");
    	return strResponse;
    } 
    
    @SuppressWarnings("unchecked")
	@RequestMapping(value = urlPrefix+"/updatePostData", method = {RequestMethod.GET,RequestMethod.POST})
    public String updatePostData(@RequestParam String dataString, @RequestParam String mode, Model model, Principal principal) {
    	String strResponse = null;
    	Map<String,Object> postData = null;
    	logger.info("Calling Service updatePostData....");
    	ObjectMapper jsonMapper = new ObjectMapper();
    	jsonMapper.enable(SerializationFeature.INDENT_OUTPUT);
    	try {
    		postData = new ObjectMapper().readValue(dataString, Map.class);
    	}catch(Exception ex) {
    		ex.getMessage();  
        	logger.error("Error in updatePostData...." + ex.getMessage());
    	}
    	if(mode != null && mode.equals("add"))
    		strResponse = mainService.addPostData(postData);
    	else
    		strResponse = mainService.updatePostData(postData);
    	logger.info("Sending Response updatePostData....");
    	return strResponse;
    }    

	@RequestMapping(value = urlPrefix+"/deletePostData", method = {RequestMethod.GET,RequestMethod.POST})
    public String deletePostData(@RequestParam String postId, Model model, Principal principal) {
    	String strResponse = null;
    	logger.info("Calling Service deletePostData....");
   		strResponse = mainService.deletePostData(postId);
    	logger.info("Sending Response deletePostData....");
    	return strResponse;
    } 
	
    @RequestMapping(value = urlPrefix+"/getDomainDataList", method = {RequestMethod.GET,RequestMethod.POST})
    public String getDomainDataList(Model model, Principal principal) {
    	String strResponse = null;
    	System.out.println("inside getDomainDataList function");
    	
    	logger.info("Calling Service getDomainDataList....");
    	Map<String,Object> entity = mainService.getDomainDataList();

    	ObjectMapper jsonMapper = new ObjectMapper();
    	jsonMapper.enable(SerializationFeature.INDENT_OUTPUT);
    	try {
    		strResponse = jsonMapper.writeValueAsString(entity);
    	}catch(Exception ex) {
    		ex.getMessage();  
        	logger.error("Error in getDomainDataList...." + ex.getMessage());
    	}
    	logger.info("Sending Response getDomainDataList....");
    	return strResponse;
    } 
	
}
