package com.rb.core.controller;

import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.rb.core.dto.MasterSheetDataDto;
import com.rb.core.service.EverGlobalDataService;

@CrossOrigin(origins = "http://localhost")
@RestController
public class EverGlobalDataContoller {
	
	@Autowired
	EverGlobalDataService mainService;
	
	final static String urlPrefix = "/API/Master";
	private static final Logger logger = LoggerFactory.getLogger(EverGlobalDataContoller.class);
	
    @RequestMapping(value = urlPrefix+"/Country", method = {RequestMethod.GET,RequestMethod.POST})
    public String getCountry(Model model, Principal principal) {
    	String strResponse = null;
    	Map<String, Object> responseObj = null;
    	
    	logger.info("Calling Service getCountry....");
    	Map<String, Object> entityList = mainService.getCountry();

    	logger.info("Creating Response getCountry....");
    	responseObj = createResponseObject(entityList, "Country");
    	
    	ObjectMapper jsonMapper = new ObjectMapper();
    	jsonMapper.enable(SerializationFeature.INDENT_OUTPUT);
    	try {
    		strResponse = jsonMapper.writeValueAsString(responseObj);
    	}catch(Exception ex) {
    		ex.getMessage();  
        	logger.error("Error in getCountry...." + ex.getMessage());
    	}
    	logger.info("Sending Response getCountry....");
    	return strResponse;
    } 
    
    @RequestMapping(value = urlPrefix+"/CountryCode", method = {RequestMethod.GET,RequestMethod.POST})
    public String getCountryCode(Model model, Principal principal) {
    	String strResponse = null;
    	Map<String, Object> responseObj = null;
    	
    	logger.info("Calling Service getCountryCode....");
    	Map<String, Object> entityList = mainService.getCountryCode();

    	logger.info("Creating Response getCountryCode....");
    	responseObj = createResponseObject(entityList, "CountryCode");
    	
    	ObjectMapper jsonMapper = new ObjectMapper();
    	jsonMapper.enable(SerializationFeature.INDENT_OUTPUT);
    	try {
    		strResponse = jsonMapper.writeValueAsString(responseObj);
    	}catch(Exception ex) {
    		ex.getMessage();  
        	logger.error("Error in getCountryCode...." + ex.getMessage());
    	}
    	logger.info("Sending Response getCountryCode....");
    	return strResponse;
    }     

    @RequestMapping(value = urlPrefix+"/GetValidUser", method = {RequestMethod.GET,RequestMethod.POST})
    public String getValidUser(Model model, Principal principal, @RequestParam String vchUserID, @RequestParam String nvrPassword) {
    	String strResponse = null;
    	Map<String, Object> responseObj = null;
    	
    	logger.info("Calling Service getValidUser....");
    	Map<String, Object> entityList = mainService.getValidUser(vchUserID,nvrPassword);

    	logger.info("Creating Response getValidUser....");
    	responseObj = createResponseObject(entityList, "UserDetails");
    	
    	ObjectMapper jsonMapper = new ObjectMapper();
    	jsonMapper.enable(SerializationFeature.INDENT_OUTPUT);
    	try {
    		strResponse = jsonMapper.writeValueAsString(responseObj);
    	}catch(Exception ex) {
    		ex.getMessage();  
        	logger.error("Error in getValidUser...." + ex.getMessage());
    	}
    	logger.info("Sending Response getValidUser.... " + strResponse);
    	return strResponse;
    } 
    
    public Map<String, Object> createResponseObject(Map<String, Object> entityList, String strEntity) {
    	Map<String, Object> responseObj = new HashMap<String, Object>();
    	if(null != entityList) {
    		@SuppressWarnings("rawtypes")
			List resultObj = (List) entityList.get("#result-set-1");
    		if(!resultObj.isEmpty()) {
	    		logger.info("Entity not empty..");
		    	Object obj = entityList.remove("#result-set-1");
		    	entityList.put(strEntity, obj);
		    	responseObj.put("Status", "true");
		    	responseObj.put("Message", strEntity + " listed successfully.");
		    	responseObj.put("Response", entityList);
	    	}else {
		    	responseObj.put("Status", "true");
		    	responseObj.put("Message", strEntity + " list Empty.");
		    	responseObj.put("Response", "");  		
	    	}    
    	}else {
	    	responseObj.put("Status", "false");
	    	responseObj.put("Message", strEntity + " Service Error. Contact Administrator.");
	    	responseObj.put("Response", "");  	    		
    	}
    	return responseObj;
    }

    @RequestMapping(value = "/getMasterSheet", method = {RequestMethod.GET,RequestMethod.POST})
    public String getEntity(@RequestParam String entityCode, Model model, Principal principal) {
    	String strResponse = null;
    	MasterSheetDataDto entity = mainService.getEntityById(entityCode);
    	try {
    		strResponse = new ObjectMapper().writeValueAsString(entity);
    	}catch(Exception ex) {
    		ex.getMessage();
    	}
    	return strResponse;    	
    }  
 
    
    @RequestMapping(value = "/deleteMasterSheet", method = {RequestMethod.GET,RequestMethod.POST})
    public String deleteEntity(@RequestParam String entityCode, Model model, Principal principal) {    
    	String strResponse = null;
    	MasterSheetDataDto entity = mainService.getEntityById(entityCode);
    	try {
    		strResponse = mainService.deleteEntity(entity.getMasterSheet());
    	}catch(Exception ex) {
    		ex.getMessage();
    	}
    	return strResponse;     	
    }
}
