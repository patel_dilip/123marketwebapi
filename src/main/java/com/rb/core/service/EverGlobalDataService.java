package com.rb.core.service;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rb.core.dao.EverGlobalDataDao;
import com.rb.core.dao.MasterSheetDataDao;
import com.rb.core.dto.MasterSheetDataDto;
import com.rb.core.model.MasterSheetBean;

@Service
public class EverGlobalDataService {

	@Autowired
	MasterSheetDataDao mstDao;
	
	@Autowired
	EverGlobalDataDao objDao;	

	String strMessage = null;
	private static final Logger logger = LoggerFactory.getLogger(EverGlobalDataService.class);
	
	public List<MasterSheetBean> getList() {
		return mstDao.findAllEntities();
	}
	
	public Map<String, Object> getCountry() {
		logger.info("Calling DAO getCountry...");
		return objDao.getCountry();
	}
	
	public Map<String, Object> getCountryCode() {
		logger.info("Calling DAO getCountryCode...");
		return objDao.getCountryCode();
	}	
	
	public Map<String, Object> getValidUser(String vchUserID, String nvrPassword) {
		logger.info("Calling DAO getValidUser...");
		return objDao.getValidUser(vchUserID,nvrPassword);
	}	
	
	public MasterSheetDataDto getEntityById(String strId) {
		return loadResponse(mstDao.findEntityById(strId));
	}

	public MasterSheetDataDto getEntityByName(String strName) {
		return loadResponse(mstDao.findEntityByName(strName));
	}

	public MasterSheetDataDto loadResponse(MasterSheetBean masterSheet) {	
		
		MasterSheetDataDto resp = new MasterSheetDataDto();
		
		return resp;
	}
	
	public String updateEntity(MasterSheetBean entity) {
		strMessage = null;
		String strResp = null;
		if (entity != null && validateEntityForUpdate(entity)) {
 		    mstDao.updateEntity(entity);
			strResp = "Success : ";
		} else {
			strResp = "Error : ";
		}
		return strResp + strMessage;
	}

	public String addEntity(MasterSheetBean entity) {
		strMessage = null;
		String strResp = null;
		if (entity != null && validateEntityForAdd(entity)) {
			mstDao.addEntity(entity);
			strResp = "Success";
		} else {
			strResp = "Error : ";
		}
		return strResp + strMessage;
	}
	

	public String deleteEntity(MasterSheetBean entity) {
		String strResp = null;
		if (entity != null) {
			mstDao.deleteEntity(entity);
			strResp = "Success";
		} else {
			strResp = "Error deleting the record";
		}
		return strResp;
	}

	boolean validateEntityForUpdate(MasterSheetBean entity) {
		boolean resp = false;
		MasterSheetBean objRT = mstDao.findEntityById(String.valueOf(entity.getId()));
		if (objRT != null) {
			if (objRT.getMasterSheetNo().equals(entity.getMasterSheetNo())) {
				resp = true;
			} else {
				if (getEntityByName(entity.getMasterSheetNo()) == null) {
					resp = true;
				} else {
					strMessage = "Master Sheet Number already exists";
				}
			}
		} else {
			strMessage = "Record does not exist";
		}
		return resp;
	}

	boolean validateEntityForAdd(MasterSheetBean entity) {
		boolean resp = false;
		MasterSheetBean objRT = mstDao.findEntityByName(entity.getMasterSheetNo());
		if (objRT == null) {
			resp = true;
		} else {
			strMessage = "Master Sheet Number already exists";
		}
		return resp;
	}
}