package com.rb.core.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rb.core.dao.EverGlobalDataDao;
import com.rb.core.dto.PostDataDto;
import com.rb.core.dto.UserDetailDataDto;
import com.rb.core.dto.UserProfileDto;


@Service
public class EverGlobalService {
	
	@Autowired
	EverGlobalDataDao objDao;	

	String strMessage = null;
	private static final Logger logger = LoggerFactory.getLogger(EverGlobalService.class);
	
	public Map<String, Object> getCountry() {
		logger.info("Calling DAO getCountry...");
		return objDao.getCountry();
	}
	
	public List<Map<String, Object>> getLatestLeads(Map<String,Object> filterData) {
		logger.info("Calling DAO getLatestLeads...");
		return objDao.getLatestLeads(filterData);
	}
	
	public Map<String, Object> getItemDetails(String itemId) {
		logger.info("Calling DAO getItemDetails...");
		return objDao.getItemDetails(itemId);
	}
	
	public UserDetailDataDto getUserDetailAndLeads(String userId){
		logger.info("Calling userIformation and  getLatestLeads...");
		Map<String,Object> entity = objDao.userInfoAndLeads(userId);
		
		if(null != entity && !entity.isEmpty()) {
			modifyForDate(entity, "dtCreatedDate");
		}
		
		return loadUserLeadResponse(entity,userId);
	}
	
	
	public void modifyForDate(Map<String,Object> entity, String key) {
		Object objInvDate = entity.get(key);
		if(null != objInvDate && !objInvDate.toString().isEmpty()) {
			java.sql.Timestamp sqlDate = (java.sql.Timestamp)objInvDate;
			entity.remove(key);
			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			String strDate = dateFormat.format(sqlDate); 
			entity.put(key, strDate);
		}		
	}
	public Map<String, Object> getCountryCode() {
		logger.info("Calling DAO getCountryCode...");
		return objDao.getCountryCode();
	}	
	
	public Map<String, Object> getValidUser(String vchUserID, String nvrPassword) {
		logger.info("Calling DAO getValidUser...");
		return objDao.getValidUser(vchUserID,nvrPassword);
	}	
	
	public UserDetailDataDto loadUserLeadResponse(Map<String,Object> userDetail,String userId) {
		UserDetailDataDto resp = new UserDetailDataDto();
		
		List<Map<String, Object>> productDetails = objDao.leadDetails(userId);
		//List<DomainBean> cities = dmDao.findAllEntities("cities");
		resp.setUserDetailSheet(userDetail);
		resp.setLeadDetails(productDetails);
		
		return resp;
	}

	public UserProfileDto getUserProfileData(String userId) {
		
		Map<String,Object> userData = new HashMap<String,Object>();
		List<Map<String,Object>> countryList = objDao.getCountryList();
		List<Map<String,Object>> stateList = objDao.getStateList();
		List<Map<String,Object>> cityList = objDao.getCityList();
		List<Map<String,Object>> phoneCC = objDao.getPhoneCC();

		if(null != userId && !userId.isEmpty()) {
			userData = objDao.getUserData(userId);
		}

		UserProfileDto resp = new UserProfileDto();
		resp.setUserProfile(userData);
		resp.setCountryList(countryList);
		resp.setStateList(stateList);
		resp.setCityList(cityList);
		resp.setPhoneCC(phoneCC);
		
		return resp;
	}

	public String addUserProfile(Map<String, Object> userData) {
		return objDao.addUserProfile(userData);
	}
	
	public String updateUserProfile(Map<String, Object> userData) {
		return objDao.updateUserProfile(userData);
	}

	public String validateUserInfo(Map<String, Object> userData) {
		return objDao.validateUserInfo(userData);
	}
	
	public Map<String, Object> validateUserInfoN(Map<String, Object> userData) {
		return objDao.validateUserInfoN(userData);
	}

	public PostDataDto getPostData(String postId) {
		
		Map<String,Object> postInfo = new HashMap<String,Object>();
		List<Map<String,Object>> categoryList = objDao.getCategoryList();

		if(null != postId && !postId.isEmpty()) {
			postInfo = objDao.getPostData(postId);
		}

		PostDataDto resp = new PostDataDto();
		resp.setPostInfo(postInfo);
		resp.setCategoryList(categoryList);
		
		return resp;
	}

	public String addPostData(Map<String, Object> postData) {
		return objDao.addPostData(postData);
	}

	public String updatePostData(Map<String, Object> postData) {
		return objDao.updatePostData(postData);
	}

	public String deletePostData(String postId) {
		return objDao.deletePostData(postId);
	}

	public Map<String, Object> getDomainDataList() {
		
		Map<String,Object> domainDataList = new HashMap<String,Object>();
		
		List<Map<String,Object>> countryList = objDao.getCountryList();
		List<Map<String,Object>> categoryList = objDao.getCategoryList();
		
		domainDataList.put("countryList", countryList);
		domainDataList.put("categoryList", categoryList);

		return domainDataList;
	}
	
}